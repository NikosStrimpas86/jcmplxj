package com.jcmplxj;

import java.util.Objects;

// TODO: Reject null references
// TODO: Replace comments with formulas, with MathML
public class Complex extends Number {
    /* --------------------- Constants --------------------- */

    /**
     * Representation of zero in the complex plane. Essentially just <i>0.0+j0.0</i>.
     */
    public static final Complex ZERO = new Complex();
    /**
     * Representation of one in the complex plane. Essentially just <i>1.0+j0.0</i>.
     */
    public static final Complex ONE = new Complex(1.0);
    /**
     * Square root of -1. Essentially just <i>0.0+j1.0</i>.
     */
    public static final Complex I = new Complex(0.0, 1.0);
    /**
     * A representation of NaN in the complex plane. Essentially just <i>NaN+jNaN</i>.
     */
    public static final Complex NaN = new Complex(Double.NaN, Double.NaN);
    /**
     * A non-finite number in the complex plane. Essentialy just <i>(+inf)+j(+inf)</i>.
     */
    public static final Complex INFINITY = new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

    /* --------------------- Members --------------------- */

    /**
     * The real part of the complex number.
     */
    private final double real;
    /**
     * The imaginary part of the complex number.
     */
    private final double imag;

    /* --------------------- Constructors --------------------- */

    /**
     * Constructs a complex number from the real part and the imaginary part
     *
     * @param real The real part of the number
     * @param imag The imaginary part of the number
     */
    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    /**
     * Default constructor for the Complex class. Constructs complex zero <i>(0.0, 0.0)</i>
     */
    public Complex() {
        this(0.0, 0.0);
    }

    /**
     * Constructs a complex number from a real number
     *
     * @param scalar The real part of the number
     */
    public Complex(double scalar) {
        this(scalar, 0.0);
    }

    /**
     * Copy constructor for the Complex class. Constructs a copy of the given number
     *
     * @param other The complex number to be copied
     * @throws NullPointerException if {@code other} is {@code null}
     */
    public Complex(Complex other) throws NullPointerException {
        Objects.requireNonNull(other);
        this.real = other.getReal();
        this.imag = other.getImag();
    }

    /* --------------------- Getters --------------------- */

    /**
     * Getter for the real part of the number
     *
     * @return The real part of the number
     */
    public double getReal() {
        return real;
    }

    /**
     * Getter for the imaginary part of the number
     *
     * @return The imaginary part of the number
     */
    public double getImag() {
        return imag;
    }

    /* --------------------- Factories --------------------- */

    public static Complex fromPolar(double r, double theta) {
        if (Double.isNaN(r) || signbit(r))
            return new Complex(Double.NaN, Double.NaN);
        if (Double.isNaN(theta)) {
            if (Double.isInfinite(r))
                return new Complex(r, theta);
            return new Complex(theta, theta);
        }
        if (Double.isInfinite(theta)) {
            if (Double.isInfinite(r))
                return new Complex(r, Double.NaN);
            return new Complex(Double.NaN, Double.NaN);
        }
        double x = r * Math.cos(theta);
        double y = r * Math.sin(theta);
        if (Double.isNaN(x))
            x = 0.0d;
        if (Double.isNaN(y))
            y = 0.0d;
        return new Complex(x, y);
    }

    public static Complex fromPolar(double r) {
        return fromPolar(r, 0.0d);
    }

    /* --------------------- Operations (non-static) --------------------- */

    public Complex add(double scalar) {
        return add(this, scalar);
    }

    public Complex add(Complex other) throws NullPointerException {
        Objects.requireNonNull(other);
        return add(this, other);
    }

    public Complex subtract(double scalar) {
        return subtract(this, scalar);
    }

    public Complex subtract(Complex other) throws NullPointerException {
        Objects.requireNonNull(other);
        return subtract(this, other);
    }

    public Complex multiply(double scalar) {
        return multiply(this, scalar);
    }

    public Complex multiply(Complex other) throws NullPointerException {
        Objects.requireNonNull(other);
        return multiply(this, other);
    }

    public Complex divide(double scalar) {
        return divide(this, scalar);
    }

    public Complex divide(Complex other) throws NullPointerException {
        Objects.requireNonNull(other);
        return divide(this, other);
    }

    /* --------------------- Functions (non-static) --------------------- */


    public boolean isNaN() {
        return Complex.isNaN(this);
    }

    public boolean isInfinite() {
        return Complex.isInfinite(this);
    }

    public boolean isFinite() {
        return Complex.isFinite(this);
    }

    /**
     * Computes the complex absolute value (also called norm, modulus, or magnitude) of a complex number.
     * <p>
     * For an object {@code z} of type {@code Complex}, errors and special cases are handled as if the
     * function is implemented as {@code Math.hypot(z.getReal(), z.getImag())}.
     * </p>
     *
     * @return The magnitude of a complex number.
     * @apiNote The exact mathematical expression of {@code z.abs()} is equal to: <i>sqrt(Re{z}<sup>2</sup>+Im{z}<sup>2</sup>)</i>.
     */
    public double abs() {
        return Complex.abs(this);
    }

    /**
     * Computes the argument (also called phase angle) of z
     * <p>
     * FOr an object {@code z} of type {@code Complex}, errors and special cases are handled as if the function is implemented as
     * {@code Math.atan2(z.getImag(), z.getReal()}.
     * </p>
     *
     * @return The phase angle of a complex number.
     * @apiNote The exact mathematical expression of {@code arg(z)} is equal to: <i>arctan2(Im{z}, Re{z})</i>.
     */
    public double arg() {
        return Complex.arg(this);
    }

    /**
     * Computes the complex conjugate of a complex number by reversing the sign of its imaginary part.
     *
     * @return The complex conjugate of a complex number.
     */
    public Complex conj() {
        return conj(this);
    }

    public Complex negate() {
        return negate(this);
    }

    /**
     * Computes the projection of z on the Riemann sphere
     * <p>
     * For most {@code z} where {@code z} an object of type {@code Complex}, {@code z.proj()==z}, but all complex infinities,
     * even numbers where one component is infinite and the other is {@code NaN},
     * become positive real infinity, `inf+j0.0` or `inf-j0.0`. The sign of the imaginary part (zero)
     * is the sign of {@code z.getImag()}.
     * </p>
     *
     * @return The projection of z on the Riemann sphere.
     */
    public Complex proj() {
        return proj(this);
    }

    public Complex sqrt() {
        return Complex.sqrt(this);
    }

    /* --------------------- Operations (static) --------------------- */

    public static Complex add(Complex lhs, double rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        return new Complex(lhs.getReal() + rhs, lhs.getImag());
    }

    public static Complex add(Complex lhs, Complex rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        Objects.requireNonNull(rhs);
        return new Complex(lhs.getReal() + rhs.getReal(), lhs.getImag() + rhs.getImag());
    }

    public static Complex subtract(Complex lhs, double rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        return new Complex(lhs.getReal() - rhs, lhs.getImag());
    }

    public static Complex subtract(Complex lhs, Complex rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        Objects.requireNonNull(rhs);
        return new Complex(lhs.getReal() - rhs.getReal(), lhs.getImag() - rhs.getImag());
    }

    public static Complex multiply(Complex lhs, double rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        return new Complex(lhs.getReal() * rhs, lhs.getImag() * rhs);
    }

    public static Complex multiply(Complex lhs, Complex rhs) throws NullPointerException {
        Objects.requireNonNull(lhs);
        Objects.requireNonNull(rhs);
        // ISO Annex G algorithm
        double a = lhs.getReal();
        double b = lhs.getImag();
        double c = rhs.getReal();
        double d = rhs.getImag();
        double x = DoublePair.fastSumOfProducts(a, c, -b, d).evaluate();
        double y = DoublePair.fastSumOfProducts(a, d, b, c).evaluate();

        if (Double.isNaN(x) && Double.isNaN(y)) {
            boolean recalc = false;

            if (Double.isInfinite(a) || Double.isInfinite(b)) {
                a = Math.copySign(Double.isInfinite(a) ? 1.0d : 0.0d, a);
                b = Math.copySign(Double.isInfinite(b) ? 1.0d : 0.0d, b);

                if (Double.isNaN(c))
                    c = Math.copySign(0.0d, c);
                if (Double.isNaN(d))
                    d = Math.copySign(0.0d, d);
                recalc = true;
            }
            if (Double.isInfinite(c) || Double.isInfinite(d)) {
                c = Math.copySign(Double.isInfinite(c) ? 1.0d : 0.0d, c);
                d = Math.copySign(Double.isInfinite(d) ? 1.0d : 0.0d, d);

                if (Double.isNaN(a))
                    a = Math.copySign(0.0d, a);
                if (Double.isNaN(b))
                    b = Math.copySign(0.0d, b);
                recalc = true;
            }
            if (!recalc && (Double.isInfinite(a * c) || Double.isInfinite(b * d) || Double.isInfinite(a * d) || Double.isInfinite(b * c))) {
                if (Double.isNaN(a))
                    a = Math.copySign(0.0d, a);
                if (Double.isNaN(b))
                    b = Math.copySign(0.0d, b);
                if (Double.isNaN(c))
                    c = Math.copySign(0.0d, c);
                if (Double.isNaN(d))
                    d = Math.copySign(0.0d, d);

                recalc = true;
            }
            if (recalc) {
                final double inf = Double.POSITIVE_INFINITY;
                x = inf * (a * c - b * d);
                y = inf * (a * d + b * c);
            }
        }

        return new Complex(x, y);
    }

    public static Complex divide(Complex lhs, double rhs) {
        Objects.requireNonNull(lhs);
        return new Complex(lhs.getReal() / rhs, lhs.getImag() / rhs);
    }

    public static Complex divide(Complex lhs, Complex rhs) {
        Objects.requireNonNull(lhs);
        Objects.requireNonNull(rhs);
        // TODO: Maybe replace this with Priest complex division
        // TODO: Test this more, especially the > or >=
        // TODO: Handle 0.0, inf, NaN.
        double a = lhs.getReal();
        double b = lhs.getImag();
        double c = rhs.getReal();
        double d = rhs.getImag();
        double e;
        double f;
        double temp;
        boolean flip = false;

        if (Math.abs(d) > Math.abs(c)) {
            temp = c;
            c = d;
            d = temp;

            temp = a;
            a = b;
            b = temp;
            flip = true;
        }

        double s = 1.0d / c;
        double t = 1.0d / (c + d * (d * s));

        if (Math.abs(d) > Math.abs(s)) {
            temp = d;
            d = s;
            s = temp;
        }

        if (Math.abs(b) >= Math.abs(s)) {
            e = t * (a + s * (b * d));
        } else if (Math.abs(b) >= Math.abs(d)) {
            e = t * (a + b * (s * d));
        } else {
            e = t * (a + d * (s * b));
        }

        if (Math.abs(a) >= Math.abs(s)) {
            f = t * (b - s * (a * d));
        } else if (Math.abs(a) >= Math.abs(d)) {
            f = t * (b - a * (s * d));
        } else {
            f = t * (b - d * (s * a));
        }

        if (flip) {
            f = -f;
        }

        return new Complex(e, f);
    }

    static Complex divisionByPriest(Complex z, Complex w) {
        Objects.requireNonNull(z);
        Objects.requireNonNull(w);
        double a = z.getReal();
        double b = z.getImag();
        double c = w.getReal();
        double d = w.getImag();
        long aa = Double.doubleToRawLongBits(a);
        long bb = Double.doubleToRawLongBits(b);
        long cc = Double.doubleToRawLongBits(c);
        long dd = Double.doubleToRawLongBits(d);
        long ha = aa >>> 32;
        long hb = bb >>> 32;
        long hc = cc >>> 32;
        long hd = dd >>> 32;
        long hz = Math.max(ha, hb);
        long hw = Math.max(hc, hd);
        long hs = (hz < 0x0720_000L && hw >= 0x3280_0000L && hw < 0x4710_0000L)
                ? ((((0x4710_0000L - hw) >>> 1) & 0xfff0_0000L) + 0x3ff0_0000L)
                : ((((hw >>> 2) - hw) + 0x6fd7_ffff) & 0xfff0_0000L);
        double s = Double.longBitsToDouble(hs << 32);
        c *= s;
        d *= s;
        double t = 1.0d / (c * c + d * d);
        c *= s;
        d *= s;
        return new Complex((a * c + b * d) * t, (b * c - a * d) * t);
    }

    /* --------------------- Functions (non-static) --------------------- */

    public static boolean isNaN(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return Double.isNaN(z.getReal()) || Double.isNaN(z.getImag());
    }

    public static boolean isInfinite(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return Double.isInfinite(z.getReal()) || Double.isInfinite(z.getImag());
    }

    public static boolean isFinite(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return Double.isFinite(z.getReal()) && Double.isFinite(z.getImag());
    }

    /**
     * Computes the complex absolute value (also called norm, modulus, or magnitude) of {@code z}.
     * <p>
     * Errors and special cases are handled as if the function is implemented as
     * {@code Math.hypot(z.getReal(), z.getImag())}.
     * </p>
     *
     * @param z The complex number
     * @return The magnitude of a complex number
     * @throws NullPointerException if {@code z} is {@code null}.
     * @apiNote The exact mathematical expression of {@code abs(z)} is equal to: <i>sqrt(Re{z}<sup>2</sup>+Im{z}<sup>2</sup>)</i>.
     */
    public static double abs(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return Math.hypot(z.getReal(), z.getImag());
    }

    /**
     * Computes the argument (also called phase angle) of {@code z}.
     * <p>
     * Errors and special cases are handled as if the function is implemented as
     * {@code Math.atan2(z.getImag(), z.getReal()}.
     * </p>
     *
     * @param z The complex number
     * @return The phase angle of {@code z}.
     * @throws NullPointerException if {@code z} is {@code null}.
     * @apiNote The exact mathematical expression of {@code arg(z)} is equal to: <i>arctan2(Im{z}, Re{z})</i>.
     */
    public static double arg(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return Math.atan2(z.getImag(), z.getReal());
    }

    /**
     * Computes the complex conjugate of {@code z} by reversing the sign of the imaginary part.
     *
     * @param z The complex number
     * @return The complex conjugate of {@code z}.
     * @throws NullPointerException if {@code z} is {@code null}.
     */
    public static Complex conj(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return new Complex(z.getReal(), -z.getImag());
    }

    public static Complex negate(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        return new Complex(-z.getReal(), -z.getImag());
    }

    /**
     * Computes the projection of z on the Riemann sphere
     * <p>
     * For most {@code z}, {@code Complex.proj(z)==z}, but all complex infinities,
     * even numbers where one component is infinite and the other is {@code NaN},
     * become positive real infinity, `inf+j0.0` or `inf-j0.0`. The sign of the imaginary part (zero)
     * is the sign of {@code z.getImag()}.
     * </p>
     *
     * @param z The complex number
     * @return The projection of z on the Riemann sphere.
     * @throws NullPointerException if {@code z} is {@code null}
     */
    public static Complex proj(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        if (Double.isInfinite(z.getReal()) || Double.isInfinite(z.getImag()))
            return new Complex(Double.POSITIVE_INFINITY, Math.copySign(0.0d, z.getImag()));
        return new Complex(z);
    }

    public static Complex sqrt(Complex z) throws NullPointerException {
        Objects.requireNonNull(z);
        double x = z.getReal();
        double y = z.getImag();

        if (Double.doubleToLongBits(x) == 0L && Double.doubleToLongBits(y) == 0L)
            return new Complex();
        if (Double.isInfinite(y))
            return new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        if (Double.isInfinite(x)) {
            if (signbit(x))
                return new Complex(Double.isNaN(y) ? y : +0.0, Math.copySign(x, y));
            else
                return new Complex(x, Double.isNaN(y) ? y : 0.0d);
        }
        if (Double.isNaN(x))
            return new Complex(x, Math.copySign(x, y));
        if (Double.isNaN(y))
            return new Complex(y, y);

        final double TWO = 2.0d;
        final double TWO_SQUARED = 2.0d * 2.0d;
        final double TWO_INV = 1.0d / 2.0d;
        final double TWO_INV_SQUARED = 1.0d / (2.0d * 2.0d);
        double s, sInvSquared;

        if (Math.abs(x) > 1.0d || Math.abs(y) > 1.0d) {
            s = TWO;
            sInvSquared = TWO_INV_SQUARED;
        } else {
            s = TWO_INV;
            sInvSquared = TWO_SQUARED;
        }

        Complex w = z.multiply(sInvSquared);
        double u = w.getReal();
        double v = w.getImag();
        double t = Math.sqrt(0.5d * (Math.abs(u) + Complex.abs(w)));

        if (t == 0.0d)
            return new Complex(0.0d, y);
        else if (u >= 0.0d) {
            double a = t;
            double b = v / (a + a);
            return new Complex(s * a, s * b);
        } else {
            double b = Math.copySign(t, v);
            double a = v / (b + b);
            return new Complex(s * a, s * b);
        }
    }

    /* --------------------- Usual overloads --------------------- */

    @Override
    public int hashCode() {
        return (Double.hashCode(this.real) / 2) + (Double.hashCode(this.imag) / 2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "(" + this.real + ", " + this.imag + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Complex other) {
            final Double re = this.real;
            final Double im = this.imag;
            return re.equals(other.getReal()) && im.equals(other.getImag());
        }
        return false;
    }

    /* --------------------- Number overloads --------------------- */

    /**
     * {@inheritDoc}
     */
    @Override
    public int intValue() {
        return (int)this.real;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long longValue() {
        return (long)this.real;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float floatValue() {
        return (float)this.real;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doubleValue() {
        return this.real;
    }

    /* --------------------- Private Stuff --------------------- */

    private static class DoublePair {

        static final DoublePair ZERO = new DoublePair(0.0d, 0.0d);

        private final double high;
        private final double low;

        DoublePair(double high, double low) {
            this.high = high;
            this.low = (low == 0.0d) ? Math.copySign(low, high) : low;
        }

        DoublePair(DoublePair other) {
            this(other.getHigh(), other.getLow());
        }

        double getHigh() {
            return this.high;
        }

        double getLow() {
            return this.low;
        }

        double evaluate() {
            return this.high + this.low;
        }

        DoublePair negate() {
            return new DoublePair(-this.high, -this.low);
        }

        static DoublePair fastSumOfProducts(double a, double b, double c, double d) {
            double ab = a * b;
            double cd = c * d;
            double high = 0.0d;
            double low = 0.0d;
            // TODO: Investigate whether signbit like functions are better for this
            if (ab >= 0.0 && cd >= 0.0)
                high = ab + cd;
            else if (ab < 0.0 && cd < 0.0)
                high = ab + cd;
            else {
                double ababs = Math.abs(ab);
                double cdabs = Math.abs(cd);

                if ((cdabs + cdabs) < ababs || (ababs + ababs) < cdabs)
                    high = ab + cd;
                else {
                    double abError = Math.fma(a, b, -ab);
                    high = Math.fma(c, d, ab);
                    low = abError;
                }
            }
            return new DoublePair(high, low);
        }
    }

    private static boolean signbit(double x) {
        return Double.doubleToRawLongBits(x) >>> 63 != 0L;
    }
}
