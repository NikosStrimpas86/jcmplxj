package com.jcmplxj;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class JCMPLXJComplexTest {

    /**
     * Negative zero
     */
    static private final double NZERO = Double.longBitsToDouble(-9223372036854775808L);


    /* --------------------- Constructors and getters --------------------- */

    @Test
    @DisplayName("Test the constructors and the getters of the Complex class")
    void complexConstructorsGetterTest() {
        System.out.println("[Complex Test]: Testing the constructors and the getters");
        // Common case of a point in the complex plane
        final double x = 1.25d;
        final double y = 5.12d;
        final Complex z = new Complex(x, y);
        assertEquals(x, z.getReal());
        assertEquals(y, z.getImag());

        // Default/empty constructor
        final Complex w = new Complex();
        assertEquals(0.0d, w.getReal());
        assertEquals(0.0d, w.getImag());

        // Real number in the complex plane
        final Complex v = new Complex(x);
        assertEquals(x, v.getReal());
        assertEquals(0.0d, v.getImag());

        // Copy constructor
        final Complex q = new Complex(z);
        assertEquals(z.getReal(), q.getReal());
        assertEquals(z.getImag(), q.getImag());
    }

    /* --------------------- Factories --------------------- */

    @Test
    @DisplayName("Test the fromPolar() factory")
    void complexFromPolarTest() {
        double[][] simpleCasesOfRealNumbers = {
                { 0.0 },
                { 1.0 },
                { 250.0 },
                { 0.0, 0.0 },
                { 1.0, 0.0 },
                { 250.0, 0.0 },
        };
        for (double[] realCoordinates : simpleCasesOfRealNumbers) {
            if (realCoordinates.length == 1) {
                assertEquals(realCoordinates[0], Complex.fromPolar(realCoordinates[0]).getReal());
            } else if (realCoordinates.length == 2) {
                var realPoint = Complex.fromPolar(realCoordinates[0], realCoordinates[1]);
                assertEquals(realCoordinates[0], realPoint.getReal());
                assertEquals(0.0, realPoint.getImag());
            }
        }

    }

    /* --------------------- Functions --------------------- */

    @Test
    @DisplayName("Test the isNan() method/function")
    void complexIsNaNTest() {
        // TODO: Should we follow ISO C here on the inf + NaN pair?
        System.out.println("[Complex Test]: Testing the isNaN() function");
        final Complex z = new Complex(943.0d, 434.88d);
        assertFalse(z.isNaN());

        final Complex w = new Complex(Double.NaN, 0.21d);
        assertTrue(w.isNaN());

        final Complex v = new Complex(0.12d, Double.NaN);
        assertTrue(v.isNaN());

        final Complex q = new Complex(Double.NaN, Double.NaN);
        assertTrue(q.isNaN());

        assertTrue(Complex.isNaN(Complex.NaN));

        assertFalse(Complex.isNaN(Complex.ZERO));
        assertFalse(Complex.isNaN(Complex.ONE));
        assertFalse(Complex.isNaN(Complex.I));
        assertFalse(Complex.isNaN(Complex.INFINITY));
    }

    @Test
    @DisplayName("Test the isInfinite() method/function")
    void complexIsInfiniteTest() {
        // TODO: Test the NaN + inf pair
        System.out.println("[Complex Test]: Testing the isInfinite() function");
        final Complex z = new Complex(333.333d, 888.32d);
        assertFalse(z.isInfinite());

        final Complex w1 = new Complex(Double.POSITIVE_INFINITY, 0.32d);
        assertTrue(w1.isInfinite());
        final Complex w2 = new Complex(Double.NEGATIVE_INFINITY, 0.23d);
        assertTrue(w2.isInfinite());

        final Complex v1 = new Complex(0.444d, Double.POSITIVE_INFINITY);
        assertTrue(v1.isInfinite());
        final Complex v2 = new Complex(0.4413d, Double.NEGATIVE_INFINITY);
        assertTrue(v2.isInfinite());

        assertTrue(Double.isInfinite(-Double.POSITIVE_INFINITY));

        final Complex q1 = new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        assertTrue(q1.isInfinite());
        final Complex q2 = new Complex(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        assertTrue(q2.isInfinite());
        final Complex q3 = new Complex(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY);
        assertTrue(q3.isInfinite());
        final Complex q4 = new Complex(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        assertTrue(q4.isInfinite());

        assertTrue(Complex.isInfinite(Complex.INFINITY));

        assertFalse(Complex.isInfinite(Complex.ZERO));
        assertFalse(Complex.isInfinite(Complex.ONE));
        assertFalse(Complex.isInfinite(Complex.I));
        assertFalse(Complex.isInfinite(Complex.NaN));
    }

    @Test
    @DisplayName("Test the isFinite() method/function")
    void complexIsFiniteTest() {
        System.out.println("[Complex Test]: Testing the isFinite() function");
        final Complex z1 = new Complex(1.34d, 9999.0d);
        assertTrue(z1.isFinite());
        final Complex z2 = new Complex(-44.32d, 48483.0d);
        assertTrue(z2.isFinite());
        final Complex z3 = new Complex(73.32184d, -8888.88d);
        assertTrue(z3.isFinite());
        final Complex z4 = new Complex(-22.11d, -21.21d);
        assertTrue(z4.isFinite());
        final Complex z5 = new Complex();
        assertTrue(z5.isFinite());
        final Complex z6 = new Complex(500.0d);
        assertTrue(z6.isFinite());

        final Complex w1 = new Complex(Double.POSITIVE_INFINITY);
        assertFalse(w1.isFinite());
        final Complex w2 = new Complex(Double.NEGATIVE_INFINITY, -23.3d);
        assertFalse(w2.isFinite());
        final Complex w3 = new Complex(42.2d, Double.POSITIVE_INFINITY);
        assertFalse(w3.isFinite());
        final Complex w4 = new Complex(0.0d, Double.NEGATIVE_INFINITY);
        assertFalse(w4.isFinite());
        final Complex w5 = new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        assertFalse(w5.isFinite());
        final Complex w6 = new Complex(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        assertFalse(w6.isFinite());
        final Complex w7 = new Complex(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY);
        assertFalse(w7.isFinite());
        final Complex w8 = new Complex(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        assertFalse(w8.isFinite());

        final Complex v1 = new Complex(Double.NaN);
        assertFalse(v1.isFinite());
        final Complex v2 = new Complex(0.11d, Double.NaN);
        assertFalse(v2.isFinite());
        final Complex v3 = new Complex(Double.NaN, Double.NaN);
        assertFalse(v3.isFinite());

        final Complex q1 = new Complex(Double.POSITIVE_INFINITY, Double.NaN);
        assertFalse(q1.isFinite());
        final Complex q2 = new Complex(Double.NEGATIVE_INFINITY, Double.NaN);
        assertFalse(q2.isFinite());
        final Complex q3 = new Complex(Double.NaN, Double.POSITIVE_INFINITY);
        assertFalse(q3.isFinite());
        final Complex q4 = new Complex(Double.NaN, Double.NEGATIVE_INFINITY);
        assertFalse(q4.isFinite());

        assertTrue(Complex.isFinite(Complex.ZERO));
        assertTrue(Complex.isFinite(Complex.ONE));
        assertTrue(Complex.isFinite(Complex.I));

        assertFalse(Complex.isFinite(Complex.INFINITY));
        assertFalse(Complex.isFinite(Complex.NaN));
    }

    @Test
    @DisplayName("Test the abs() function/method")
    void complexAbsTest() {
        // hypot(x, y), hypot(y, x), and hypot(x, -y) are equivalent.
        Complex z = new Complex(3.0, 4.0);
        double zabs = Complex.abs(z);
        assertEquals(5.0d, zabs);
        Complex w = new Complex(z.getImag(), z.getReal());
        double wabs = Complex.abs(w);
        assertEquals(zabs, wabs);
        Complex v = z.conj();
        double vabs = Complex.abs(v);
        assertEquals(zabs, vabs);

        // hypot(x, +/-0) returns the absolute value of x, if x is not a NaN.
        Complex scalar = new Complex(5.3210d);
        assertEquals(Math.abs(5.3210d), scalar.abs());

        // hypot(+/-inf, y) returns +inf, even if y is a NaN.
        Complex rinf1 = new Complex(Double.POSITIVE_INFINITY, 4.0d);
        Complex rinf2 = new Complex(Double.NEGATIVE_INFINITY, 4.0d);
        Complex rinf3 = new Complex(Double.POSITIVE_INFINITY, Double.NaN);
        Complex rinf4 = new Complex(Double.NEGATIVE_INFINITY, Double.NaN);
        assertTrue(Double.isInfinite(rinf1.abs()));
        assertTrue(Double.isInfinite(rinf2.abs()));
        assertTrue(Double.isInfinite(rinf3.abs()));
        assertTrue(Double.isInfinite(rinf4.abs()));

        // hypot(x, NaN) returns a NaN, if x is not +/-inf.
        Complex imagNaN = new Complex(123.23d, Double.NaN);
        assertTrue(Double.isNaN(Complex.abs(imagNaN)));


        double[][] casesThatYieldInfinity = {
                { Double.POSITIVE_INFINITY, 1.0 },
                { Double.POSITIVE_INFINITY, -1.0 },
                { Double.POSITIVE_INFINITY, 0.0 },
                { Double.POSITIVE_INFINITY, NZERO },
                { Double.POSITIVE_INFINITY, Double.MAX_VALUE },
                { Double.POSITIVE_INFINITY, -Double.MAX_VALUE },
                { Double.POSITIVE_INFINITY, Double.MIN_NORMAL },
                { Double.POSITIVE_INFINITY, -Double.MIN_NORMAL },
                { Double.POSITIVE_INFINITY, Double.MIN_VALUE },
                { Double.POSITIVE_INFINITY, -Double.MIN_VALUE },
                { 1.0, Double.POSITIVE_INFINITY },
                { -1.0, Double.POSITIVE_INFINITY },
                { 0.0, Double.POSITIVE_INFINITY },
                { NZERO, Double.POSITIVE_INFINITY },
                { Double.MAX_VALUE, Double.POSITIVE_INFINITY },
                { -Double.MAX_VALUE, Double.POSITIVE_INFINITY },
                { Double.MIN_NORMAL, Double.POSITIVE_INFINITY },
                { -Double.MIN_NORMAL, Double.POSITIVE_INFINITY },
                { Double.MIN_VALUE, Double.POSITIVE_INFINITY },
                { -Double.MIN_VALUE, Double.POSITIVE_INFINITY },
                { Double.NEGATIVE_INFINITY, 1.0 },
                { Double.NEGATIVE_INFINITY, -1.0 },
                { Double.NEGATIVE_INFINITY, 0.0 },
                { Double.NEGATIVE_INFINITY, NZERO },
                { Double.NEGATIVE_INFINITY, Double.MAX_VALUE },
                { Double.NEGATIVE_INFINITY, -Double.MAX_VALUE },
                { Double.NEGATIVE_INFINITY, Double.MIN_NORMAL },
                { Double.NEGATIVE_INFINITY, -Double.MIN_NORMAL },
                { Double.NEGATIVE_INFINITY, Double.MIN_VALUE },
                { Double.NEGATIVE_INFINITY, -Double.MIN_VALUE },
                { 1.0, Double.NEGATIVE_INFINITY },
                { -1.0, Double.NEGATIVE_INFINITY },
                { 0.0, Double.NEGATIVE_INFINITY },
                { NZERO, Double.NEGATIVE_INFINITY },
                { Double.MAX_VALUE, Double.NEGATIVE_INFINITY },
                { -Double.MAX_VALUE, Double.NEGATIVE_INFINITY },
                { Double.MIN_NORMAL, Double.NEGATIVE_INFINITY },
                { -Double.MIN_NORMAL, Double.NEGATIVE_INFINITY },
                { Double.MIN_VALUE, Double.NEGATIVE_INFINITY },
                { -Double.MIN_VALUE, Double.NEGATIVE_INFINITY },
                { Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY },
                { Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY },
                { Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY },
                { Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY },
                { Double.POSITIVE_INFINITY, Double.NaN },
                { Double.NEGATIVE_INFINITY, Double.NaN },
                { Double.NaN, Double.POSITIVE_INFINITY },
                { Double.NaN, Double.NEGATIVE_INFINITY },
                // TODO: Add SNaN cases (if possible)
        };

        for (double[] infinityCases : casesThatYieldInfinity) {
            double res = Complex.abs(new Complex(infinityCases[0], infinityCases[1]));
            assertTrue(Double.isInfinite(res));
            assertTrue(Math.signum(res) > 0.0d);
        }

        double[][] casesThatYieldNaN = {
                { 0.0, Double.NaN },
                { NZERO, Double.NaN },
                { Double.MAX_VALUE, Double.NaN },
                { -Double.MAX_VALUE, Double.NaN },
                { Double.MIN_NORMAL, Double.NaN },
                { -Double.MIN_NORMAL, Double.NaN },
                { Double.MIN_VALUE, Double.NaN },
                { -Double.MIN_VALUE, Double.NaN },
                { 9.49, Double.NaN },
                { -9.49, Double.NaN },
                { Double.NaN, 0.0 },
                { Double.NaN, NZERO },
                { Double.NaN, Double.MAX_VALUE },
                { Double.NaN, -Double.MAX_VALUE },
                { Double.NaN, Double.MIN_NORMAL },
                { Double.NaN, -Double.MIN_NORMAL },
                { Double.NaN, Double.MIN_VALUE },
                { Double.NaN, -Double.MIN_VALUE },
                { Double.NaN, 9.49 },
                { Double.NaN, -9.49 },
                { Double.NaN, Double.NaN },
                // NaN with different payloads taken from OpenJDK source code
                { Double.longBitsToDouble(0x7ff0000000000001L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0xfff0000000000001L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0x7ff8555555555555L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0xfff8555555555555L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0x7fffffffffffffffL), 1.0, Double.NaN },
                { Double.longBitsToDouble(0xffffffffffffffffL), 1.0, Double.NaN },
                { Double.longBitsToDouble(0x7ffdeadbeef00000L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0xfffdeadbeef00000L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0x7ffcafebabe00000L), 1.0, Double.NaN },
                { Double.longBitsToDouble(0xfffcafebabe00000L), 1.0, Double.NaN },
                // TODO: Add SNaN cases (if possible)
        };

        for (double[] nanCases : casesThatYieldNaN) {
            double res = Complex.abs(new Complex(nanCases[0], nanCases[1]));
            assertTrue(Double.isNaN(res));
        }

        double[][] casesThatYieldNumbers = {
                { 0.7, 1.2, 1.3892443989449804508432547041028554, 1 },
                { 3.0, 4.0, 5.0, 1 }
        };

        for (double[] normalCases : casesThatYieldNumbers) {
            double res = Complex.abs(new Complex(normalCases[0], normalCases[1]));
            assertEquals(normalCases[2], res, normalCases[3] * Math.ulp(normalCases[2]));
        }
    }

    /* --------------------- Usual overloads --------------------- */

    @Test
    @DisplayName("Test hashCode()")
    void complexHashCodeTest() {
        System.out.println("[Complex Test]: Testing the hashCode() method");
        final Complex z1 = new Complex(-153.3232d, -200.125d);
        final Complex z2 = new Complex(z1);
        assertEquals(z1, z2);
        assertEquals(z1.hashCode(), z2.hashCode());
    }

    @Test
    @DisplayName("Test toString()")
    void complexToStringTest() {
        System.out.println("[Complex Test]: Testing the toString() method");
        final double x = 3.24d;
        final double y = 123.32d;
        final Complex z = new Complex(x, y);
        final String expected = "(" + x + ", " + y + ")";
        assertEquals(expected, z.toString());
    }

    @Test
    @DisplayName("Test equals()")
    void complexEqualsTest() {
        System.out.println("[Complex Test]: Testing the equals() method");
        final Complex z1 = new Complex(743.4444d, -938.355d);
        final Complex z2 = new Complex(z1);
        final Complex z3 = new Complex(4444.32d, 999.33d);
        assertEquals(z1, z2);
        assertNotEquals(z1, z3);
    }

    /* --------------------- Number overloads --------------------- */

    @Test
    @DisplayName("Test intValue()")
    void complexIntValueTest() {
        System.out.println("[Complex Test]: Testing the intValue() method");
        final Complex z = new Complex(3.0d, 4.0d);
        assertEquals(3, z.intValue());
        // TODO: More testing
    }

    @Test
    @DisplayName("Test longValue()")
    void complexLongValueTest() {
        System.out.println("[Complex Test]: Testing the longValue() method");
        final Complex z = new Complex(3.0d, 4.0d);
        assertEquals(3L, z.longValue());
    }

    @Test
    @DisplayName("Test floatValue()")
    void complexFloatValueTest() {
        System.out.println("[Complex Test]: Testing the floatValue() method");
        final Complex z = new Complex(3.0d, 4.0d);
        assertEquals(3.0f, z.floatValue());
    }

    @Test
    @DisplayName("Test doubleValue()")
    void complexDoubleValueTest() {
        System.out.println("[Complex Test]: Testing the doubleValue() method");
        final Complex z = new Complex(3.0d, 4.0d);
        assertEquals(3.0d, z.doubleValue());
    }
}
